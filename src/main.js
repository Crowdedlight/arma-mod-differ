import Vue from 'vue'
import App from './App.vue'

import { BootstrapVue, BootstrapVueIcons, VBTooltipPlugin  } from 'bootstrap-vue'
import Clipboard from 'v-clipboard'

import './assets/custom.scss'

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(Clipboard)
Vue.use(VBTooltipPlugin)

new Vue({
  render: h => h(App),
}).$mount('#app')
